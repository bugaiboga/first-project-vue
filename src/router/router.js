import { createRouter, createWebHashHistory } from "vue-router";
import HomePage from "@/pages/HomePage";
import FavoritePage from "@/pages/FavoritePage";

const routes = [
  {
    path: "/",
    component: HomePage,
  },
  {
    path: "/favorite",
    component: FavoritePage,
  },
];

const router = createRouter({
  routes,
  history: createWebHashHistory(process.env.BASE_URL),
});

export default router;
