export const bascketModule = {
  state: () => ({
    items: [],
    loading: false,
    totalPrice: 0,
  }),
  mutations: {
    setItems(state, items) {
      state.items = items;
    },
    deleteItem(state, item) {
      state.items = state.items.filter((element) => element.id !== item.id);
    },
    setPrice(state, item) {
      state.totalPrice += item;
    },
    minusPrice(state, item) {
      state.totalPrice -= item;
    },

    setLoading(state, loading) {
      state.loading = loading;
    },
  },
  getters: {
    getItems(state) {
      return state.items;
    },
    getTotalPrice(state) {
      return state.totalPrice;
    },
  },
  actions: {
    handleBascket({ state, commit }, arg) {
      commit("setLoading", true);
      if (state.items.find((item) => item.id === arg.id)) {
        commit("minusPrice", arg.price);
        commit("deleteItem", arg);
      } else {
        commit("setPrice", arg.price);
        commit("setItems", [...state.items, arg]);
      }
      commit("setLoading", false);
    },
  },
  namespaced: true,
};
