import { createStore } from "vuex";
import { favoriteModule } from "./favoriteModule";
import { bascketModule } from "./bascketModule";

export default createStore({
  modules: {
    favorite: favoriteModule,
    bascket: bascketModule,
  },
});
