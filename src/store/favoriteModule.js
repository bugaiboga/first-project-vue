export const favoriteModule = {
  state: () => ({
    items: [],
    loading: false,
  }),
  mutations: {
    setItems(state, items) {
      state.items = items;
    },
    deleteItem(state, item) {
      state.items = state.items.filter((element) => element.id !== item.id);
    },
    setLoading(state, loading) {
      state.loading = loading;
    },
  },
  getters: {
    getItems(state) {
      return state.items;
    },
  },
  actions: {
    handleFavorite({ state, commit }, arg) {
      commit("setLoading", true);
      if (state.items.find((item) => item.id === arg.id)) {
        commit("deleteItem", arg);
      } else {
        commit("setItems", [...state.items, arg]);
      }
      commit("setLoading", false);
    },
  },
  namespaced: true,
};
